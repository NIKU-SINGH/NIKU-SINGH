 <img src="https://komarev.com/ghpvc/?username=niku-singh&label=Profile%20views&color=0e75b6&style=flat" alt="niku-singh" />
 
 <img src="https://github.com/NIKU-SINGH/NIKU-SINGH/blob/main/assets/banner.png" />

### Hey,there <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px" height="30px"> I'm Niku Singh

#### Prefinal year Student at NIT Jalandhar,Opensource Contributor,Blockchain enthusiast

---

- 👨🏻‍💻 Pronouns: He/Him
- 🔭 I am currently exploring Bitcoin dev Space
- 📽️ Some of my projects are
  - [SAAVE Finance ](https://github.com/SAAVE-Finance)
  - [HawkEye](https://github.com/NIKU-SINGH/Hawkeye)
  - [Homey](https://github.com/NIKU-SINGH/rentals-frontend)
  - [Go Auth System](https://github.com/NIKU-SINGH/auth-system)
  - [Readers Hub](https://github.com/NIKU-SINGH/readers-hub-frontend)
- 🖥️ Check my open source contributions [here](https://github.com/NIKU-SINGH/My-Open-Source-Contri)
- ☕ Hobbies: Like to read books, watch football and watch movies
- ⚡ Fun fact: The only funny thing about my life is my situation

---

## Connect with me

[<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/twitter.svg" />](https://twitter.com/niku_singh_)
[<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/instagram.svg" />](https://instagram.com/_niku__singh_)
[<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/linkedin.svg" />](https://www.linkedin.com/in/niku-singh)
[<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/devrant.svg" />](https://devrant.com/users/Nikus)

## Read My Blogs Here

[<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/medium.svg" />](https://medium.com/@nikusingh)

<!-- [<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/DEVCommunity.svg" />](https://dev.to/nikusingh)
[<img height="35" width="35" src="https://cdn.jsdelivr.net/npm/simple-icons@v5/icons/hashnode.svg" />](https://hashnode.com/@Sukin) -->

## Languages and tools that I use

<p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=javascript,typescript,python,c,cpp,rust,go" />
  </a>
</p>
<p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=next,react,express,tailwind,nodejs,actix,bootstrap,materialui,graphql" />
  </a>
</p>
<p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=git,github,gitlab,docker,figma,netlify" />
  </a>
</p>
<p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=heroku,firebase,vercel,mysql,mongo,postgres,linux" />
  </a>
</p>

---

## Achievements

- ###  **[Mentor](https://mentorship.lfx.linuxfoundation.org/project/6904ed62-2022-4451-bbc7-6bc0f940586f) at LFX'23 Hyperledger Foundation**

  - As a mentor for LFX'23 Hyperledger Mentorship Programme, my mentee and I are updating Hyperledger's onboarding documentation and start here guide to provide new community members with a quicker and deeper understanding of blockchain projects. Our goal is to create a unified, easy-to-access resource, including a searchable dashboard for community events.

- ### **LFX'22 Mentee Hyperledger Foundation**
  -  Designed website mockups for the Start-here-Hyperledger site, which aims at
improving user engagement and making it easier to onboard new contributors.
  - Worked on the latest technologies for building the UI of the site.
<!-- - Runner up of [MIT Bitcoin Expo Forkscanner Track](https://devpost.com/software/hawkeye-28azjf) -->
- ###  **Winner of [ETHforAll QuickNode track](https://devfolio.co/projects/saave-61c5)**
  - SAAVE is a DeFi investing app that offers a simple UI, one-click solutions for various investment strategies, and a 3.75% APY on stablecoins. It also provides educational content and rewards users with NFTs for learning.

- ###  **Winner of Gitlab [Q3 Hackathon](https://forum.gitlab.com/t/announcing-gitlabs-q3-2021-hackathon-winners/60356)**
  - Gitlab conducts a quarterly Hackathon to introduce new contributors to open-source software and help them start their journey with Gitlab project. I was a winner of the Q3 Gitlab Hackathon and have since then been contributing to their codebase regularly.

---

### Profile Stats

<!-- <img height="150px" align="left" src="https://github-readme-stats.vercel.app/api?username=niku-singh&show_icons=true&theme=radical" alt="niku-singh" />
<img height= "150px" align="left" src="https://github-readme-streak-stats.herokuapp.com/?user=niku-singh&theme=radical" alt="niku-singh" /> -->
